﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class UserLeaveDays
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string LeaveType { get; set; }
        public int? LeaveDays { get; set; }
        public int? RemainingDays { get; set; }
    }
}
