﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class Departments
    {
        public long Id { get; set; }
        public string DepartmentName { get; set; }
    }
}
