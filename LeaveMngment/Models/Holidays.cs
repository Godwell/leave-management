﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class Holidays
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
    }
}
