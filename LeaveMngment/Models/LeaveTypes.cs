﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class LeaveTypes
    {
        public long Id { get; set; }
        public string LeaveType { get; set; }
        public string Description { get; set; }
    }
}
