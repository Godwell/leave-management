﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class LeaveForm
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public DateTime? LeaveFrom { get; set; }
        public DateTime? LeaveTo { get; set; }
        public int? NoOfDays { get; set; }
        public string LeaveType { get; set; }
        public string Reason { get; set; }
        public string Title { get; set; }
        public int? Approved { get; set; }
    }
}
