﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class LeaveSchedule
    {
        public long Id { get; set; }
        public DateTime? LeaveFrom { get; set; }
        public DateTime? LeaveTo { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public int? Days { get; set; }
        public string Maker { get; set; }
        public int? Status { get; set; }
    }
}
