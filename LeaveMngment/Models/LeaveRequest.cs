﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class LeaveRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string LeaveType { get; set; }
        public int? LeaveDays { get; set; }
    }
}
