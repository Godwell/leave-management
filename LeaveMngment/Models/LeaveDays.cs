﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class LeaveDays
    {
        public long Id { get; set; }
        public string LeaveType { get; set; }
        public string EmploymentType { get; set; }
        public int? Days { get; set; }
    }
}
