﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Models.DAL
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetModel();

        T GetModelByID(long modelId);

        void InsertModel(T model);

        void DeleteModel(long modelId);

        void UpdateModel(T model);

        void Save();
    }
}
