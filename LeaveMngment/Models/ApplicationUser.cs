﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Names { get; set; }
        public string IdNumber { get; set; }
        public string  Department { get; set; }
        public bool FirstTimeLogin { get; set; }
        public int LoginAttempts { get; set; }
        public string EmploymentType { get; set; }
        public string Title { get; set; }
        public string Gender { get; set; }
    }
}
