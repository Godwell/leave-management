﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class EmploymentType
    {
        public long Id { get; set; }
        public string EmploymentType1 { get; set; }
        public string Description { get; set; }
    }
}
