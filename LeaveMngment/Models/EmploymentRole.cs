﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class EmploymentRole
    {
        public long Id { get; set; }
        public string EmploymentRole1 { get; set; }
        public string RoleDescription { get; set; }
    }
}
