﻿using System;
using System.Collections.Generic;

namespace LeaveMngment.Models
{
    public partial class CalcLeaveDay
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? Days { get; set; }
        public string LeaveType { get; set; }
    }
}
