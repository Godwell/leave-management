﻿using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Common
{
    public class Commons
    {
        private LeaveManagementContext _db;
        public Commons(LeaveManagementContext db)
        {
            _db = db;
        }
        public string GetTitle(string username)
        {
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == username);
            string Title = usr.Title;
            return Title;
        }
    }
}
