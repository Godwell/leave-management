﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class LeaveTypesValidations : AbstractValidator<LeaveTypes>
    {
        public LeaveTypesValidations()
        {
            RuleFor(x => x.LeaveType).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
        }
    }
}
