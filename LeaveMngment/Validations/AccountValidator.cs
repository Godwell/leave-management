﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class AccountValidator : AbstractValidator<AspNetUsers>
    {
        public AccountValidator()
        {
            RuleFor(x => x.UserName).NotNull();
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.PhoneNumber).MaximumLength(10).MinimumLength(7);
            RuleFor(x => x.Department).NotNull();
            RuleFor(x => x.IdNumber).NotNull();
        }
    }
}
