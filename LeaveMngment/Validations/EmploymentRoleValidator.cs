﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class EmploymentRoleValidator : AbstractValidator<EmploymentRole>
    {
        public EmploymentRoleValidator()
        {
            RuleFor(x => x.EmploymentRole1).NotEmpty();
            RuleFor(x => x.RoleDescription).NotEmpty();
        }
    }
}
