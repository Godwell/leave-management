﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class HolidaysValidator : AbstractValidator<Holidays>
    {
        public HolidaysValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Date).NotEmpty();
        }
    }
}
