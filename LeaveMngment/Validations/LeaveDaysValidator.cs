﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class LeaveDaysValidator : AbstractValidator<LeaveDays>
    {
        public LeaveDaysValidator()
        {
            RuleFor(x => x.LeaveType).NotEmpty();
        }
    }
}
