﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class DepartmentValidator : AbstractValidator<Departments>
    {
        public DepartmentValidator()
        {
            RuleFor(x => x.DepartmentName).NotEmpty();
        }
    }
}
