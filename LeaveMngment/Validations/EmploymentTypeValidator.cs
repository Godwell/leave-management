﻿using FluentValidation;
using LeaveMngment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaveMngment.Validations
{
    public class EmploymentTypeValidator : AbstractValidator<EmploymentType>
    {
        public EmploymentTypeValidator()
        {
            RuleFor(x => x.EmploymentType1).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
        }
    }
}
