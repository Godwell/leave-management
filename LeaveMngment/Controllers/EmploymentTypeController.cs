﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class EmploymentTypeController : Controller
    {
        private IRepository<EmploymentType> interfaceObj;
        public EmploymentTypeController()
        {
            this.interfaceObj = new Repository<EmploymentType>();
        }
        // GET: EmploymentType
        public ActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }

        // GET: EmploymentType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmploymentType/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmploymentType employmentType)
        {
            if (!ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    interfaceObj.InsertModel(employmentType);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Saved successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    
                }
            }
            return View();

        }

        // GET: EmploymentType/Edit/5
        public ActionResult Edit(int id)
        {
            EmploymentType employmentType = interfaceObj.GetModelByID(id);
            return View(employmentType);
        }

        // POST: EmploymentType/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EmploymentType employmentType)
        {
            if (!ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    // TODO: Add update logic here
                    interfaceObj.UpdateModel(employmentType);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Edited successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();
            
        }

        //GET: EmploymentType/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                //EmploymentType employmentType = interfaceObj.GetModelByID(id);
                interfaceObj.DeleteModel(id);
                interfaceObj.Save();
                TempData["successmessage"] = "Deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
            //return View();
        }
    }
}