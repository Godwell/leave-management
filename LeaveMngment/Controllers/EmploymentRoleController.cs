﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class EmploymentRoleController : Controller
    {
        private IRepository<EmploymentRole> interfaceObj;
        public EmploymentRoleController()
        {
            this.interfaceObj = new Repository<EmploymentRole>();
        }
        // GET: EmploymentRole
        public ActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }

        // GET: EmploymentRole/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmploymentRole/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmploymentRole employmentRole)
        {
            if (!ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    // TODO: Add insert logic here
                    interfaceObj.InsertModel(employmentRole);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Saved successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }

        // GET: EmploymentRole/Edit/5
        public ActionResult Edit(int id)
        {
            EmploymentRole employmentRole = interfaceObj.GetModelByID(id);
            return View(employmentRole);
        }

        // POST: EmploymentRole/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EmploymentRole employmentRole)
        {
            if (!ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    // TODO: Add update logic here
                    interfaceObj.UpdateModel(employmentRole);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Edited successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }

        // GET: EmploymentRole/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                interfaceObj.DeleteModel(id);
                interfaceObj.Save();
                TempData["successmessage"] = "Deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}