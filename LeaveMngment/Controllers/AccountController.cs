﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Common;
using LeaveMngment.Data;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using X.PagedList;

namespace LeaveMngment.Controllers
{
    
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private IRepository<AspNetUsers> interfaceObj;
        private LeaveManagementContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, LeaveManagementContext db, ApplicationDbContext applicationDbContext, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this.interfaceObj = new Repository<AspNetUsers>();
            _db = db;
            _applicationDbContext = applicationDbContext;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var user_det = _db.AspNetUsers.ToList();
            
            if (Status == "UserName")
            {
                user_det = user_det.Where(s => s.UserName.StartsWith(search)).ToList();
                var usrs = user_det.ToPagedList(pageNumber, 25);
                return View(usrs);
            }
            if (Status == "Email")
            {
                user_det = user_det.Where(s => s.Email.StartsWith(search)).ToList();
                var usr = user_det.ToPagedList(pageNumber, 25);
                return View(usr);
            }
            return View(user_det.ToPagedList(pageNumber, 25));
        }
        public IActionResult CreateUser()
        {
            List<Departments> departments = _db.Departments.ToList();
            ViewBag.Types = departments;
            List<EmploymentType> employmentTypes = _db.EmploymentType.ToList();
            ViewBag.Emp = employmentTypes;
            List<EmploymentRole> employmentRoles = _db.EmploymentRole.ToList();
            ViewBag.role = employmentRoles;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(int id, AspNetUsers aspNetUsers)
        {
            var err = "";
            var usrs = _db.AspNetUsers.Where(x => x.Email == aspNetUsers.Email).ToList();
            if (usrs.Count == 0)
            {
                //Guid g;
                //g = Guid.NewGuid();
                if (ModelState.IsValid)
                {
                    string pass = "Wellcome254@";
                    //"Prot_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                    var user = new ApplicationUser { UserName = aspNetUsers.UserName, Email = aspNetUsers.Email, PhoneNumber = aspNetUsers.PhoneNumber, Department = aspNetUsers.Department, IdNumber = aspNetUsers.IdNumber, EmploymentType = aspNetUsers.EmploymentType, Title = aspNetUsers.Title, Gender = aspNetUsers.Gender, Names = aspNetUsers.Names };
                    var result = await _userManager.CreateAsync(user, pass);

                    if (result.Succeeded)
                    {
                        var leaveTypes = _db.LeaveDays.Where(x => x.EmploymentType == aspNetUsers.EmploymentType).ToList();

                        if (aspNetUsers.Gender == "Male")
                        {
                            foreach (var lT in leaveTypes)
                            {
                                string lTypes = lT.LeaveType;
                                if (lTypes != "Maternity")
                                {
                                    UserLeaveDays userLeaveDays = new UserLeaveDays();
                                    userLeaveDays.UserName = aspNetUsers.UserName;
                                    userLeaveDays.Email = aspNetUsers.Email;
                                    userLeaveDays.Department = aspNetUsers.Department;
                                    userLeaveDays.LeaveType = lTypes;
                                    userLeaveDays.LeaveDays = lT.Days;
                                    userLeaveDays.RemainingDays = lT.Days;
                                    _db.UserLeaveDays.Add(userLeaveDays);
                                    _db.SaveChanges();
                                }
                            }
                        }
                        if (aspNetUsers.Gender == "Female")
                        {
                            foreach (var lT in leaveTypes)
                            {
                                string lTypes = lT.LeaveType;
                                if (lTypes != "Paternity")
                                {
                                    UserLeaveDays userLeaveDays = new UserLeaveDays();
                                    userLeaveDays.UserName = aspNetUsers.UserName;
                                    userLeaveDays.Email = aspNetUsers.Email;
                                    userLeaveDays.Department = aspNetUsers.Department;
                                    userLeaveDays.LeaveType = lTypes;
                                    userLeaveDays.LeaveDays = lT.Days;
                                    userLeaveDays.RemainingDays = lT.Days;
                                    _db.UserLeaveDays.Add(userLeaveDays);
                                    _db.SaveChanges();
                                }
                            }
                        }
                        TempData["successmessage"] = "Saved successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    foreach (var error in result.Errors)
                    {
                        err = error.Description;
                    }
                }


            }
            return View();



        }
        public IActionResult EditUser(int id)
        {
            List<Departments> departments = _db.Departments.ToList();
            ViewBag.Types = departments;
            AspNetUsers aspNetUsers = interfaceObj.GetModelByID(id);
            return View(aspNetUsers);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditUser(int id, AspNetUsers aspNetUsers)
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult Logout()
        {
            return RedirectToAction("Login");
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            Commons c = new Commons(_db);
            c.GetTitle(loginViewModel.UserName);
            var result = await _signInManager.PasswordSignInAsync(loginViewModel.UserName, loginViewModel.Password, loginViewModel.RememberMe, false);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            //var y = _db.AspNetUsers.OrderByDescending(x => x.Title).FirstOrDefault(x => x.Email == loginViewModel.Email && x.PasswordHash == loginViewModel.Password);
            //if(y == null)
            //{
            //    TempData["errmessage"] = "Wrong Credentials!!!";
            //}
            //else
            //{
            //    if(y.Title == "HR")
            //    {
            //        return RedirectToAction("Index");
            //    }
            //    if (y.Title == "Manager")
            //    {
            //        return RedirectToAction("Index", "User", new {id = y.Email, area = "Management" });
            //    }
            //    if (y.Title == "Employee")
            //    {
            //        return RedirectToAction("Index");
            //    }

            //}
            return View();
        }
    }
}