﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class HolidaysController : Controller
    {
        private IRepository<Holidays> interfaceObj;
        public HolidaysController()
        {
            this.interfaceObj = new Repository<Holidays>();
        }
        // GET: Holidays
        public ActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }

        // GET: Holidays/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Holidays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Holidays/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Holidays holidays)
        {
            if (!ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    // TODO: Add insert logic here
                    interfaceObj.InsertModel(holidays);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Saved successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }

        // GET: Holidays/Edit/5
        public ActionResult Edit(int id)
        {
            Holidays holidays = interfaceObj.GetModelByID(id);
            return View(holidays);
        }

        // POST: Holidays/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Holidays holidays)
        {
            try
            {
                interfaceObj.UpdateModel(holidays);
                interfaceObj.Save();
                // TODO: Add update logic here
                TempData["successmessage"] = "Edited successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Holidays/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                //EmploymentType employmentType = interfaceObj.GetModelByID(id);
                interfaceObj.DeleteModel(id);
                interfaceObj.Save();
                TempData["successmessage"] = "Deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}