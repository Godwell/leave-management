﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Common;
using LeaveMngment.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace LeaveMngment.Controllers
{
    public class UserLeaveController : Controller
    {
        private LeaveManagementContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserLeaveController(LeaveManagementContext db, IHttpContextAccessor httpContextAccessor)
        {
            _db = db;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index(string currentFilter, string Status, string search, int? page)
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var leave_days = _db.UserLeaveDays.ToList();
            if (usr.Title == "HR")
            {
                
                if (Status == "Email")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search)).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                if (Status == "Department")
                {
                    leave_days = leave_days.Where(s => s.Department.StartsWith(search)).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                return View(leave_days.ToPagedList(pageNumber, 25));
            }
            if (usr.Title == "Manager")
            {
                if (Status == "Email")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.Department == usr.Department).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                if (Status == "Department")
                {
                    leave_days = leave_days.Where(s => s.Department.StartsWith(search) && s.Department == usr.Department).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                leave_days = leave_days.Where(s => s.UserName == userId).ToList();
                return View(leave_days.ToPagedList(pageNumber, 25));
            }
            if (usr.Title == "Employee")
            {
                if (Status == "Email")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.UserName == userId).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                if (Status == "Department")
                {
                    leave_days = leave_days.Where(s => s.Department.StartsWith(search) && s.UserName == userId).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                leave_days = leave_days.Where(s => s.UserName == userId).ToList();
                return View(leave_days.ToPagedList(pageNumber, 25));
            }
            return View();
            
        }
        public IActionResult ScheduleIndex(string currentFilter, string Status, string search, int? page)
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;

            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if(usr.Title == "HR")
            {
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                var leave_days = _db.UserLeaveDays.ToList();
                if (Status == "Email")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.LeaveType == "Annual").ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                if (Status == "Department")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.LeaveType == "Annual").ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                leave_days = leave_days.Where(s=>s.LeaveType == "Annual").ToList();
                var lDayss = leave_days.ToPagedList(pageNumber, 25);
                return View(lDayss);
            }
            if(usr.Title == "Manager")
            {
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                var leave_days = _db.UserLeaveDays.ToList();
                if (Status == "Email")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.LeaveType == "Annual" && s.Department == usr.Department).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                if (Status == "Department")
                {
                    leave_days = leave_days.Where(s => s.Department.StartsWith(search) && s.LeaveType == "Annual" && s.Department == usr.Department).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }

                leave_days = leave_days.Where(s => s.LeaveType == "Annual" && s.Department == usr.Department).ToList();
                var lDayss = leave_days.ToPagedList(pageNumber, 25);
                return View(lDayss);
            }
            if (usr.Title == "Employee")
            {
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                var leave_days = _db.UserLeaveDays.ToList();
                if (Status == "Email")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.LeaveType == "Annual" && s.UserName == userId).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                if (Status == "Department")
                {
                    leave_days = leave_days.Where(s => s.Email.StartsWith(search) && s.LeaveType == "Annual" && s.UserName == userId).ToList();
                    var lDays = leave_days.ToPagedList(pageNumber, 25);
                    return View(lDays);
                }
                leave_days = leave_days.Where(s => s.LeaveType == "Annual" && s.UserName == userId).ToList();
                var lDayss = leave_days.ToPagedList(pageNumber, 25);
                return View(lDayss);
            }
            return View();
        }
        //view schedule
        public IActionResult ViewSchedule()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if (usr.Title == "HR")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 1).ToList();
                return View(schedule);
            }
            if (usr.Title == "Manager")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 1 && x.Department == usr.Department).ToList();
                return View(schedule);
            }
            return View();
        }
        public IActionResult CreateSchedule(int id)
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateSchedule(int id, LeaveSchedule leaveSchedule)
        {
            var lDays = _db.UserLeaveDays.OrderByDescending(x => x.Id).FirstOrDefault(x => x.Id == id);
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;

            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if (usr.Title == "HR")
            {
                LeaveSchedule leave = new LeaveSchedule();
                leave.LeaveFrom = leaveSchedule.LeaveFrom;
                leave.LeaveTo = leaveSchedule.LeaveTo;
                leave.Days = leaveSchedule.Days;
                leave.Department = lDays.Department;
                leave.Email = lDays.Email;
                leave.Status =(int) MyEnums.StatusOption.Added;
                leave.Maker = usr.UserName;
                _db.LeaveSchedule.Add(leave);
                _db.SaveChanges();
                TempData["successmessage"] = "Schedule Created successfully";
                return RedirectToAction("ViewSchedule");
            }
            if(usr.Title == "Manager")
            {
                LeaveSchedule leave = new LeaveSchedule();
                leave.LeaveFrom = leaveSchedule.LeaveFrom;
                leave.LeaveTo = leaveSchedule.LeaveTo;
                leave.Days = leaveSchedule.Days;
                leave.Department = lDays.Department;
                leave.Email = lDays.Email;
                leave.Status = (int)MyEnums.StatusOption.Added;
                leave.Maker = usr.UserName;
                _db.LeaveSchedule.Add(leave);
                _db.SaveChanges();
                TempData["successmessage"] = "Schedule Created successfully";
                return RedirectToAction("ViewSchedule");
            }
            return View();
        }
        public IActionResult ViewUpproved()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if (usr.Title == "HR")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 1 && x.Maker != usr.UserName).ToList();
                return View(schedule);
            }
            if (usr.Title == "Manager")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 1 && x.Department == usr.Department && x.Maker != usr.UserName).ToList();
                return View(schedule);
            }
            return View();
        }
        public IActionResult ViewUnaproved()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if (usr.Title == "HR")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 1 && x.Maker != usr.UserName).ToList();
                return View(schedule);
            }
            if (usr.Title == "Manager")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 1 && x.Department == usr.Department && x.Maker != usr.UserName).ToList();
                return View(schedule);
            }
            return View();
        }
        public IActionResult ViewApproved()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if (usr.Title == "HR")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 3).ToList();
                return View(schedule);
            }
            if (usr.Title == "Manager")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 3 && x.Department == usr.Department).ToList();
                return View(schedule);
            }
            return View();
        }
        public IActionResult ViewRejected()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.UserName).FirstOrDefault(x => x.UserName == userId);
            if (usr.Title == "HR")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 4).ToList();
                return View(schedule);
            }
            if (usr.Title == "Manager")
            {
                var schedule = _db.LeaveSchedule.Where(x => x.Status == 4 && x.Department == usr.Department).ToList();
                return View(schedule);
            }
            return View();
        }
        public IActionResult Approve(long id)
        {
            LeaveSchedule leaveSchedule = _db.LeaveSchedule.Find(id);
            
            UserLeaveDays leavedays = _db.UserLeaveDays.OrderByDescending(x => x.Id).FirstOrDefault(x => x.Email == leaveSchedule.Email && x.LeaveType == "Annual");
            if(leaveSchedule.Days > leavedays.RemainingDays)
            {
                TempData["successmessage"] = "Scheduled Leave Days are greater than User Leave Days";
                return RedirectToAction("ViewUpproved");
            }
            else
            {
                leaveSchedule.Status = (int)MyEnums.StatusOption.Approved;
                _db.Entry(leaveSchedule).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();
                leavedays.RemainingDays = leavedays.RemainingDays - leaveSchedule.Days;
                _db.Entry(leavedays).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();
                TempData["successmessage"] = "Schedule Successfully Approved";
                return RedirectToAction("ViewApproved");
            }
            
        }
        public IActionResult Reject(long id)
        {
            LeaveSchedule leaveSchedule = _db.LeaveSchedule.Find(id);
            leaveSchedule.Status = (int)MyEnums.StatusOption.Rejected;
            _db.Entry(leaveSchedule).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _db.SaveChanges();

            TempData["successmessage"] = "Schedule Rejected";
            return RedirectToAction("ViewUnaproved");
        }
    }
    
}