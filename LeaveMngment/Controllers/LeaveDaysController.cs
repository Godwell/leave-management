﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class LeaveDaysController : Controller
    {
        private IRepository<LeaveDays> interfaceObj;
        private LeaveManagementContext _db;
        public LeaveDaysController(LeaveManagementContext db)
        {
            _db = db;
            this.interfaceObj = new Repository<LeaveDays>();
        }
        // GET: LeaveDays
        public ActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }

        // GET: LeaveDays/Create
        public ActionResult Create()
        {
            List<LeaveTypes> leaveTypes = _db.LeaveTypes.ToList();
            List<EmploymentType> employmentTypes = _db.EmploymentType.ToList();
            ViewBag.Types = leaveTypes;
            ViewBag.Employee = employmentTypes;
            
            return View();
        }

        // POST: LeaveDays/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeaveDays leaveDays)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add insert logic here
                    interfaceObj.InsertModel(leaveDays);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Saved successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }

        // GET: LeaveDays/Edit/5
        public ActionResult Edit(int id)
        {
            List<LeaveTypes> leaveTypes = _db.LeaveTypes.ToList();
            List<EmploymentType> employmentTypes = _db.EmploymentType.ToList();
            ViewBag.Types = leaveTypes;
            ViewBag.Employee = employmentTypes;
            LeaveDays leaveDays = interfaceObj.GetModelByID(id);
            return View(leaveDays);
        }

        // POST: LeaveDays/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, LeaveDays leaveDays)
        {
            try
            {
                interfaceObj.UpdateModel(leaveDays);
                interfaceObj.Save();
                // TODO: Add update logic here
                TempData["successmessage"] = "Edited successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveDays/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                //EmploymentType employmentType = interfaceObj.GetModelByID(id);
                interfaceObj.DeleteModel(id);
                interfaceObj.Save();
                TempData["successmessage"] = "Deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}