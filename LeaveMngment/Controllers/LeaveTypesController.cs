﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LeaveMngment.Controllers
{
    public class LeaveTypesController : Controller
    {
        private IRepository<LeaveTypes> interfaceObj;
        public LeaveTypesController()
        {
            this.interfaceObj = new Repository<LeaveTypes>();
        }
        // GET: LeaveTypes
        public ActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }

        // GET: LeaveTypes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LeaveTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeaveTypes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeaveTypes leaveTypes)
        {
            if (! ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    // TODO: Add insert logic here
                    interfaceObj.InsertModel(leaveTypes);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Saved successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();

        }

        // GET: LeaveTypes/Edit/5
        public ActionResult Edit(int id)
        {
            LeaveTypes leaveTypes = interfaceObj.GetModelByID(id);
            return View(leaveTypes);
        }

        // POST: LeaveTypes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, LeaveTypes leaveTypes)
        {
            try
            {
                interfaceObj.UpdateModel(leaveTypes);
                interfaceObj.Save();
                // TODO: Add update logic here
                TempData["successmessage"] = "Edited successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //GET: LeaveTypes/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                interfaceObj.DeleteModel(id);
                interfaceObj.Save();
                TempData["successmessage"] = "Deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: LeaveTypes/Delete/5

    }
}