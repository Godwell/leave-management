﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Common;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class LeaveFormController : Controller
    {
        private IRepository<LeaveForm> interfaceObj;
        private LeaveManagementContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public LeaveFormController(LeaveManagementContext db, IHttpContextAccessor httpContextAccessor)
        {
            this.interfaceObj = new Repository<LeaveForm>();
            _db = db;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var leaveForm = _db.LeaveForm.ToList();
            leaveForm = leaveForm.Where(s =>s.Name == userId && s.Approved == 1 || s.Approved == 2).ToList();
            return View(leaveForm);
        }
        public IActionResult Approved()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var leaveForm = _db.LeaveForm.ToList();
            leaveForm = leaveForm.Where(s => s.Name == userId && s.Approved == 3).ToList();
            return View(leaveForm);
        }
        public IActionResult Rejected()
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var leaveForm = _db.LeaveForm.ToList();
            leaveForm = leaveForm.Where(s => s.Name == userId && s.Approved == 4).ToList();
            return View(leaveForm);
        }
        public IActionResult CreateLeave()
        {
            List<LeaveTypes> leaveTypes = _db.LeaveTypes.ToList();
            ViewBag.Types = leaveTypes;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLeave(LeaveForm leaveForm)
        {
            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;
            var usr = _db.AspNetUsers.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId);
            var leavedays = _db.UserLeaveDays.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId);
            if(leaveForm.LeaveType == "Annual")
            {
                var anual = _db.UserLeaveDays.OrderByDescending(x => x.Id).FirstOrDefault(x => x.UserName == userId && x.LeaveType == "Annual");
                int? days = anual.RemainingDays;
                if(leaveForm.NoOfDays > days)
                {
                    TempData["successmessage"] = "You dont have Enough Leave Days";
                    return RedirectToAction(nameof(CreateLeave));
                }
                else
                {
                    leaveForm.Name = userId;
                    leaveForm.Department = usr.Department;
                    leaveForm.Title = usr.Title;
                    if (leaveForm.Title == "HR")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Approved;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    if (leaveForm.Title == "Manager")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Pending;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    if (leaveForm.Title == "Employee")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Added;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            if (leaveForm.LeaveType == "Paternity")
            {
                if(usr.Gender == "Female")
                {
                    TempData["successmessage"] = "Cannot Apply for Paternity Leave";
                    return RedirectToAction(nameof(CreateLeave));
                }
                else
                {
                    leaveForm.Name = userId;
                    leaveForm.Department = usr.Department;
                    leaveForm.Title = usr.Title;
                    if (leaveForm.Title == "HR")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Approved;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    if (leaveForm.Title == "Manager")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Pending;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    if (leaveForm.Title == "Employee")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Added;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            if (leaveForm.LeaveType == "Maternity")
            {
                if(usr.Gender == "Male")
                {
                    TempData["successmessage"] = "Cannot Apply for Maternity Leave";
                    return RedirectToAction(nameof(CreateLeave));
                }
                else
                {
                    leaveForm.Name = userId;
                    leaveForm.Department = usr.Department;
                    leaveForm.Title = usr.Title;
                    if (leaveForm.Title == "HR")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Approved;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    if (leaveForm.Title == "Manager")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Pending;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                    if (leaveForm.Title == "Employee")
                    {
                        leaveForm.Approved = (int)MyEnums.StatusOption.Added;
                        interfaceObj.InsertModel(leaveForm);
                        interfaceObj.Save();
                        TempData["successmessage"] = "Applied Leave successfully";
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            if (leaveForm.LeaveType == "Sick")
            {
                leaveForm.Name = userId;
                leaveForm.Department = usr.Department;
                leaveForm.Title = usr.Title;
                if (leaveForm.Title == "HR")
                {
                    leaveForm.Approved = (int)MyEnums.StatusOption.Approved;
                    interfaceObj.InsertModel(leaveForm);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Applied Leave successfully";
                    return RedirectToAction(nameof(Index));
                }
                if (leaveForm.Title == "Manager")
                {
                    leaveForm.Approved = (int)MyEnums.StatusOption.Pending;
                    interfaceObj.InsertModel(leaveForm);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Applied Leave successfully";
                    return RedirectToAction(nameof(Index));
                }
                if (leaveForm.Title == "Employee")
                {
                    leaveForm.Approved = (int)MyEnums.StatusOption.Added;
                    interfaceObj.InsertModel(leaveForm);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Applied Leave successfully";
                    return RedirectToAction(nameof(Index));
                }
            }
            
            return View();
        }
        public ActionResult ApproveLeave()
        {
            return View();
        }
        public ActionResult RejectLeave()
        {
            return View();
        }
    }
}