﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class LeaveRequestController : Controller
    {
        private IRepository<LeaveRequest> interfaceObj;
        public LeaveRequestController()
        {
            this.interfaceObj = new Repository<LeaveRequest>();
        }
        public IActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }
    }
}