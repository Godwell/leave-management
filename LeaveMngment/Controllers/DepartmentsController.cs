﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LeaveMngment.Models;
using LeaveMngment.Models.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveMngment.Controllers
{
    public class DepartmentsController : Controller
    {
        private IRepository<Departments> interfaceObj;
        public DepartmentsController()
        {
            this.interfaceObj = new Repository<Departments>();
        }
        // GET: Departments
        public ActionResult Index()
        {
            return View(from m in interfaceObj.GetModel() select m);
        }

        // GET: Departments/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Departments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Departments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Departments departments)
        {
            if (!ModelState.IsValid)
            {

            }
            else
            {
                try
                {
                    // TODO: Add insert logic here
                    interfaceObj.InsertModel(departments);
                    interfaceObj.Save();
                    TempData["successmessage"] = "Saved successfully";
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            return View();
            
        }

        // GET: Departments/Edit/5
        public ActionResult Edit(int id)
        {
            Departments departments = interfaceObj.GetModelByID(id);
            return View(departments);
        }

        // POST: Departments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Departments departments)
        {
            try
            {
                interfaceObj.UpdateModel(departments);
                interfaceObj.Save();
                // TODO: Add update logic here
                TempData["successmessage"] = "Edited successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Departments/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                //EmploymentType employmentType = interfaceObj.GetModelByID(id);
                interfaceObj.DeleteModel(id);
                interfaceObj.Save();
                TempData["successmessage"] = "Deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        
    }
}